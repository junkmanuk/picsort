﻿namespace PicSort
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Security.Cryptography;
    using System.Linq;

    public class Sorter
    {
        Dictionary<string, Dictionary<string, List<string>>> hashlist = new Dictionary<string, Dictionary<string, List<string>>>();
        List<String> dupes = new List<String>();
        int totalCount;
        int fileCount = 0;

        internal void Dedupe(string dir)
        {
            Console.WriteLine("Counting files in directory...");

            // Sort files in this directory
            totalCount = Directory.EnumerateFiles(dir, "*", SearchOption.AllDirectories).Count();

            // Run the dedupe check
            DoDedupe(dir);

            // delete all the found duplicates
            foreach (var d in dupes)
            {
                try
                {
                    Console.WriteLine("Deleting: " + d);
                    File.Delete(d);
                }
                catch (Exception ex)
                {
                    Console.WriteLine("Couldn't delete " + d);
                }
            }
        }

        internal void DoDedupe(string dir)
        {
            Console.WriteLine("Processing dir:" + dir);
            long dirFileCount = 0;
            var start = DateTime.Now.Ticks;

            // Sort files in this directory
            foreach (var file in Directory.EnumerateFiles(dir))
            {
                FileInfo fi = new FileInfo(file);
                string hash = Convert.ToBase64String(MD5.Create().ComputeHash(fi.OpenRead()));

                // Use the first two characters of the hash to locate the hash list
                // Create if it doesn't already exist
                string hashlistId = hash.Substring(0, 1);
                Dictionary<string, List<string>> files;
                if (hashlist.ContainsKey(hashlistId))
                {
                    files = hashlist[hashlistId];
                }
                else
                {
                    files = new Dictionary<string, List<string>>();
                    hashlist.Add(hashlistId, files);
                }

                if (!files.ContainsKey(hash))
                {
                    files.Add(hash, new List<string>());
                }
                else
                {
                    foreach (var fn in files[hash])
                    {
                        FileInfo fd = new FileInfo(fn);
                        if (Util.FilesAreEqual_OneByte(fi, fd))
                        {
                            Console.WriteLine("Binary duplicate: " + fi.Name);
                            Console.WriteLine(fd.FullName);
                            Console.WriteLine(fi.FullName + " (will delete)");
                            dupes.Add(fi.FullName);
                            break;
                        }
                    }
                }

                files[hash].Add(fi.FullName);
                fileCount++;
                dirFileCount++;
            }

            Console.Write(string.Format("{0}/{1} unique files indexed, {2} duplicates found ", fileCount, totalCount, dupes.Count));
            if (dirFileCount > 0)
            {
                var avTime = (DateTime.Now.Ticks - start) / dirFileCount / TimeSpan.TicksPerMillisecond;
                Console.WriteLine(string.Format("({0} file[1} avg {2}ms)", dirFileCount, dirFileCount > 1 ? "s" : "", avTime));
            }
            else
            {
                Console.WriteLine("(No files found)");
            }

            // Sort sub directories
            foreach (var d in Directory.EnumerateDirectories(dir))
            {
                DoDedupe(d);
            }
        }

        internal void Sort(string dir, string target)
        {
            // Sort files in this directory
            foreach (var file in Directory.EnumerateFiles(dir))
            {
                try
                {
                    FileInfo fi = new FileInfo(file);
                    string newDir = string.Format("{0}\\{1:0000}\\{2:00}\\General", target, fi.LastWriteTime.Year, fi.LastWriteTime.Month);
                    string newPath = string.Format("{0}\\{1}", newDir, fi.Name);
                    Console.WriteLine("Move file {0} to {1}", fi.Name, newPath);
                    Directory.CreateDirectory(newDir);

                    if (File.Exists(newPath))
                    {
                        FileInfo newFile = new FileInfo(newPath);
                        if (Util.FilesAreEqual_OneByte(fi, newFile))
                        {
                            File.Delete(fi.FullName);
                        }
                        else
                        {
                            int counter = 1;
                            while (File.Exists(newPath))
                            {
                                newPath = string.Format("{0}\\{1}{2:00}{3}", Path.GetDirectoryName(newFile.FullName), Path.GetFileNameWithoutExtension(newFile.FullName), counter, Path.GetExtension(newFile.FullName));
                                counter++;
                            }
                            File.Move(fi.FullName, newPath);
                        }

                    }
                    else
                    {
                        File.Move(fi.FullName, newPath);
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine("Can't move: " + file);
                }
            }

            // Sort sub directories
            foreach (var d in Directory.EnumerateDirectories(dir))
            {
                Console.WriteLine("Sorting dir: " + d);
                Sort(d, target);
            }

            // Delete the directory
            Directory.Delete(dir);
        }

    }
}
