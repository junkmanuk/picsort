﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PicSort
{
    class Program
    {
        static void Main(string[] args)
        {
            Sorter s = new Sorter();
            switch (args[0])
            {
                case "dedupe":
                    s.Dedupe(args[1]);
                    break;
                case "sort":
                    s.Sort(args[1], args[2]);
                    break;
            }
        }
    }
}
