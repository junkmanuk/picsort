﻿namespace PicSort
{
    using System;
    using System.IO;
    using System.Security.Cryptography;

    public class Util
    {
        int BYTES_TO_READ = sizeof(Int64);

        public static bool FilesAreEqual_OneByte(FileInfo first, FileInfo second)
        {
            if (first.Length != second.Length)
                return false;

            using (FileStream fs1 = first.OpenRead())
            using (FileStream fs2 = second.OpenRead())
            {
                for (int i = 0; i < first.Length; i++)
                {
                    if (fs1.ReadByte() != fs2.ReadByte())
                        return false;
                }
            }

            return true;
        }
    }
}
